const express = require("express");
const app = express();
const bodyParser = require("body-Parser");
const morgan = require("morgan");
const cors = require("cors");
const userRouter = require("./routes/userRoutes");
const connect = require("./connect");
require("dotenv").config();
const notFound = require("./middleware/not-found");
const globalErrorHandlerMiddleware = require("./middleware/global-error-handler");

// console.log(process.env);

//Middlewares
app.use(bodyParser.json({ limit: "32mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "32mb", extended: true }));
app.use(cors());
// app.use(express.json());
app.use(morgan("dev"));
app.use("/api/v1/users", userRouter);

app.use("*", notFound);
app.use(globalErrorHandlerMiddleware);

const port = process.env.PORT || 5000;

//Database connection must be successful before server is triggered
//Because the server needs the database connection to be successful in order for it to get and post data

const start = async () => {
  try {
    await connect(process.env.CONNECTION_URL);
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}`);
    });
  } catch (error) {
    console.log(error);
  }
};

start();
