//Handles the try/catch and passes the error into next which then triggers the global error middleware
const asyncWrapper = (asyncFn) => {
  return async (req, res, next) => {
    try {
      await asyncFn(req, res, next);
    } catch (error) {
      next(error);
    }
  };
};

module.exports = asyncWrapper;
