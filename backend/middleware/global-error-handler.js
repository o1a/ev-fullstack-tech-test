const { CustomAPIError } = require("../errors/custom-errClass");
//Checks if the err object is an instance of the CustomerAPIError class
//If it is the statusCode and the message will be unque and will be of the error object created by the CustomAPIError Class

const globalErrorHandlerMiddleware = (err, req, res, next) => {
  if (err instanceof CustomAPIError) {
    return res.status(err.statusCode).json({ message: err.message });
  }

  // return res.status(500).json({ message: err });

  //If the error is not an instance of CustomAPIError then we just send this generic error back
  return res
    .status(500)
    .json({ message: "Something went wrong, try again later" });
};

module.exports = globalErrorHandlerMiddleware;
