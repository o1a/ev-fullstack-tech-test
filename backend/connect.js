const mongoose = require("mongoose");

//We make use of the connectionString from mongoDB which is stored securely
//inside our .env file to connect to the database

const connect = (url) => {
  return mongoose
    .connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      console.log("Database connection successful");
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = connect;
