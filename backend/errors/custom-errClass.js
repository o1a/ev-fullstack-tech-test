//Custom class for creating unique error object instances
class CustomAPIError extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
  }
}

//Function which will be called inside our async controllers with the unique message and statusCode.
//This will trigger our Class to run
const createCustomError = (msg, statusCode) => {
  return new CustomAPIError(msg, statusCode);
};

module.exports = { createCustomError, CustomAPIError };
