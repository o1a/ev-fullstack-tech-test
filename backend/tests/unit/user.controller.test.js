const userController = require("../../controllers/userControllers");
const userModel = require("../../models/UsersModel");
const httpMocks = require("node-mocks-http");
const newUser = require("../mock-data/new-user.json");

userModel.create = jest.fn();

let req, res, next;
beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = null;
});

describe("userController.createUser", () => {
  beforeEach(() => {
    req.body = newUser;
  });
  it("should have a createUser function", () => {
    expect(typeof userController.createUser).toBe("function");
  });
  it("should call userModel.create", () => {
    userController.createUser(req, res, next);
    expect(userModel.create).toBeCalledWith(newUser);
  });
  // it("should return 201 response code", () => {
  //   userController.createUser(req, res, next);
  //   expect(res.statusCode).toBe(200);
  //   expect(res._isEndCalled()).toBeTruthy();
  // });
  // it("should return json body in response", () => {
  //   userModel.create.mockReturnValue(newUser);
  //   userController.createUser(req, res, next);
  //   expect(res._getJSONData()).toStrictEqual(newUser);
  // });
});
