const express = require("express");

const Router = express.Router();

const {
  getAllUsers,
  createUser,
  updateUser,
  deleteUser,
} = require("../controllers/userControllers");

Router.route("/").get(getAllUsers).post(createUser);
Router.route("/:id").patch(updateUser).delete(deleteUser);

module.exports = Router;
