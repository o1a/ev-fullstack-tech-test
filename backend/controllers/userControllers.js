// const mongoose = require("mongoose");
const users = require("../models/UsersModel");
const asyncWrapper = require("../middleware/async");
const { createCustomError } = require("../errors/custom-errClass");

//Controllers - try/catch removed to ensure DRY and instead the controllers
//are wrapped with asyncWrapper which handles the try/catch
const getAllUsers = asyncWrapper(async (req, res) => {
  const { search } = req.query;
  const queryObject = {};

  if (search) {
    queryObject.name = { $regex: search, $options: "i" };
  }

  const allUsers = await users.find(queryObject);
  res.status(200).json(allUsers);
});

const createUser = asyncWrapper(async (req, res, next) => {
  const newUser = await users.create(req.body);
  res.status(201).json(newUser);
});

const updateUser = asyncWrapper(async (req, res, next) => {
  const user = await users.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!user) {
    return next(createCustomError(`No User with id ${req.params.id}`, 404));
  }

  res.status(200).json(user);
});

const deleteUser = asyncWrapper(async (req, res, next) => {
  const user = await users.findByIdAndDelete(req.params.id);

  if (!user) {
    return next(createCustomError(`No User with id ${req.params.id}`, 404));
  }
  res.status(200).json({ user: null, status: "success" });
});

module.exports = { getAllUsers, createUser, updateUser, deleteUser };
