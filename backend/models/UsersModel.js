const mongoose = require("mongoose");
const validator = require("validator");

//Schema

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "A name must be provided"],
    trim: true,
    unique: true,
    maxlength: [40, "Name cannot be more than 40 characters"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "An Email must be provided"],
    lowercase: true,
    unique: true,
    validate: [validator.isEmail, "Invalid Email"],
  },
  createdDate: {
    type: String,
    default: new Date().toDateString(),
  },
  company: {
    type: String,
    required: [true, "A company must be provided"],
    trim: true,
    maxlength: [20, "company cannot be more than 20 characters"],
  },
  imageCover: {
    type: String,
    // required: [true, "A profile picture is required"],
  },
});

//Model
const User = mongoose.model("User", UserSchema);

//Model exported into the controllers - to be used for interaction with
//the database - Create, Read, Update, Delete
module.exports = User;
