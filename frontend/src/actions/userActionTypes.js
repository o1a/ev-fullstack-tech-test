import {
  CREATE,
  FETCH_ALL,
  UPDATE,
  DELETE,
  SEARCH,
  FILTER,
  CLEAR,
  LOADING,
} from "./userActions";

export const loading_Users = () => {
  return {
    type: LOADING,
  };
};

export const get_Users = (users) => {
  return {
    type: FETCH_ALL,
    payload: users,
  };
};

export const create_User = (newUser) => {
  return {
    type: CREATE,
    payload: newUser,
  };
};

export const update_User = (updatedUser) => {
  return {
    type: UPDATE,
    payload: updatedUser,
  };
};

export const delete_User = (id) => {
  return {
    type: DELETE,
    payload: id,
  };
};

export const search_users = (searchResult) => {
  return {
    type: SEARCH,
    payload: searchResult,
  };
};

export const filter_Users = (company) => {
  return {
    type: FILTER,
    payload: company,
  };
};
