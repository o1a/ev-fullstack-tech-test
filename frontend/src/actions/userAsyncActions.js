import axios from "axios";
import {
  create_User,
  get_Users,
  update_User,
  delete_User,
  loading_Users,
} from "./userActionTypes";

const url = "http://localhost:4000/api/v1/users";

export const fetchUsers = () => {
  return async (dispatch) => {
    dispatch(loading_Users());
    try {
      const { data } = await axios.get(url);
      dispatch(get_Users(data));
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const createUsers = (newUser) => {
  // console.log(newPost);
  return async (dispatch) => {
    try {
      const { data } = await axios.post(url, newUser);
      dispatch(create_User(data));
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const updateUser = (id, updatedPost) => {
  return async (dispatch) => {
    try {
      const { data } = await axios.patch(`${url}/${id}`, updatedPost);
      // console.log(data);
      dispatch(update_User(data));
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const deleteUser = (id) => {
  return async (dispatch) => {
    try {
      await axios.delete(`${url}/${id}`);
      dispatch(delete_User(id));
    } catch (error) {
      console.log(error);
    }
  };
};

// export const searchUsers = (term) => {
//   return async (dispatch) => {
//     try {
//       const { data } = await axios.get(`${url_search}`);
//     } catch (error) {}
//   };
// };
