export const LOADING = "LOADING";
export const FETCH_ALL = "FETCH_ALL";
export const CREATE = "CREATE";
export const UPDATE = "UPDATE";
export const DELETE = "DELETE";
export const SEARCH = "SEARCH";
export const FILTER = "FILTER";
