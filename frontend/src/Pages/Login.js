import React from "react";
import logo from "../images/logo.jpg";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Login = () => {
  return (
    <LoginContainer>
      <img src={logo} alt="logo" className="image-animated" />
      <Link to="/users" className="btn btn-white btn-animated">
        SEARCH FOR USERS
      </Link>
    </LoginContainer>
  );
};

export default Login;

const LoginContainer = styled.div`
  display: grid;
  place-items: center;
  height: 95vh;
  background-color: #070707;
  margin: 4px;
  clip-path: polygon(0 0, 100% 0, 100% 80vh, 0 100%);
  img {
    margin-top: 90px;
    animation: moveInLeft 0.8s ease-out;
    width: 40%;
  }
  .btn:link,
  .btn:visited {
    padding: 15px 30px;
    border-radius: 100px;
    background-color: #036d19;
    font-weight: 500;
    color: #fff;
    text-decoration: none;
    cursor: pointer;
    margin-bottom: 120px;
    transition: all 0.2s;
    position: relative;
  }

  .btn-white {
    background-color: #036d19;
    color: #777;
  }

  .btn::after {
    content: "";
    display: inline-block;
    height: 100%;
    width: 100%;
    border-radius: 100px;
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    transition: all 0.4s;
  }

  .btn-white::after {
    background-color: #036d19;
  }

  .btn:hover::after {
    transform: scaleX(1.4) scaleY(1.6);
    opacity: 0;
  }

  .btn:hover {
    transform: translateY(-3px);
    box-shadow: 0 5px 10px #0a2e36;
  }

  .btn:active {
    transform: translateY(-1px);
  }

  .btn-animated {
    animation: moveInBottom 0.8s ease-out 0.75s;
    animation-fill-mode: backwards;
  }

  @keyframes moveInBottom {
    0% {
      opacity: 0;
      transform: translateY(30px);
    }
    100% {
      opacity: 1;
      transform: translate(0);
    }
  }

  @keyframes moveInLeft {
    0% {
      opacity: 0;
      transform: translateX(-30px);
    }
    100% {
      opacity: 1;
      transform: translate(0);
    }
  }

  @media screen and (max-width: 664px) {
    clip-path: none;
    .btn:link,
    .btn:visited {
      /* margin-right: 120px; */
      /* margin-bottom: 500px; */
    }
  }
  @media screen and (max-width: 600px) {
    clip-path: none;
    .btn:link,
    .btn:visited {
      font-size: 16px;
      padding: 10px 15px;
      border-radius: 100px;
      background-color: #036d19;
    }
  }
  @media screen and (max-width: 590px) {
    clip-path: none;
    .btn:link,
    .btn:visited {
      margin-right: 150px;
    }
  }
  @media screen and (max-width: 560px) {
    clip-path: none;
    .btn:link,
    .btn:visited {
      margin-right: 0px;
    }
  }
`;
