import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Error = () => {
  return (
    <ErrorContainer>
      <ErrorPage>
        <h1>oops! it's a dead end</h1>
        <Link to="/" className="btn btn-primary">
          back home
        </Link>
      </ErrorPage>
    </ErrorContainer>
  );
};

const ErrorContainer = styled.div`
  display: flex;
  justify-content: center;
  padding: 50px 0;
`;
const ErrorPage = styled.div`
  text-align: center;
  text-transform: capitalize;

  .btn {
    text-transform: uppercase;
    letter-spacing: 3px;
    color: #c02c03;
    border: 2px solid #c02c03;
    padding: 10px 15px;
    display: inline-block;
    transition: all 0.3s linear;
    cursor: pointer;
    font-size: 0.8rem;
    background: transparent;
    border-radius: 25px;
    text-decoration: none;
    margin-top: 50px;
  }
  .btn:hover {
    background: #1ed15db0;
    color: #fff;
  }
  .btn-primary {
    background: #036d19;
    color: #fff;
    border-color: transparent;
  }
  .btn-primary:hover {
    background: #09a129;
    color: #fff;
  }
`;

export default Error;
