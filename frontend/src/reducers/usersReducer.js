import {
  CREATE,
  DELETE,
  FETCH_ALL,
  FILTER,
  LOADING,
  SEARCH,
  UPDATE,
} from "../actions/userActions";

const initialUsersState = {
  loading: true,
  users: [],
};

export const usersReducers = (state = initialUsersState, action) => {
  if (action.type === LOADING) {
    return { ...state, loading: true };
  }
  if (action.type === FETCH_ALL) {
    return { ...state, loading: false, users: action.payload };
  }
  if (action.type === CREATE) {
    return { ...state, users: [...state.users, action.payload] };
  }
  if (action.type === UPDATE) {
    const updatedUser = state.users.map((user) =>
      user._id === action.payload._id ? action.payload : user
    );
    return { ...state, users: updatedUser };
  }
  if (action.type === DELETE) {
    const newUsers = state.users.filter((user) => user._id !== action.payload);
    return { ...state, users: newUsers };
  }
  if (action.type === SEARCH) {
    return { ...state, users: action.payload };
  }
  if (action.type === FILTER) {
    const filteredUser = state.users.filter(
      (user) => user.company === action.payload
    );
    return { ...state, users: filteredUser };
  }
  return state;
};
