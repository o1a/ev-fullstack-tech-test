import axios from "axios";
import React, { useContext, useEffect, useReducer, useState } from "react";
import { useDispatch } from "react-redux";
import { search_users } from "./actions/userActionTypes";
import { fetchUsers } from "./actions/userAsyncActions";
// import data from "./data";

const url = "http://localhost:4000/api/v1/users?search=";

const UserContext = React.createContext();

export const AppProvider = ({ children }) => {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [currentID, setCurrentID] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const dispatch = useDispatch();
  const [postData, setPostData] = useState({
    name: "",
    email: "",
    company: "",
    imageCover: "",
  });
  const [loading, setLoading] = useState(true);
  // const [users, setUsers] = useState(data);

  const clearFilters = () => {
    dispatch(fetchUsers());
  };

  const openForm = () => {
    setIsFormOpen(true);
  };

  const closeForm = () => {
    setIsFormOpen(false);
  };

  const fetchSearchedUsers = async () => {
    setLoading(true);
    try {
      const { data } = await axios.get(`${url}${searchTerm}`);
      if (data) {
        dispatch(search_users(data));
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchSearchedUsers();
  }, [searchTerm]);

  return (
    <UserContext.Provider
      value={{
        isFormOpen,
        openForm,
        closeForm,
        currentID,
        setCurrentID,
        searchTerm,
        setSearchTerm,
        postData,
        setPostData,
        clearFilters,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

// custom hook
export const useGlobalContext = () => {
  return useContext(UserContext);
};
