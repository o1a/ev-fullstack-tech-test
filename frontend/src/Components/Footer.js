import React from "react";
import { BiRefresh } from "react-icons/bi";
import "./Footer.css";
import user_logo from "../images/users.jpg";
import { useDispatch } from "react-redux";
import { useGlobalContext } from "../context";

const Footer = () => {
  const { clearFilters } = useGlobalContext();
  const dispatch = useDispatch();

  return (
    <div className="footer">
      <div className="footer__left">
        <img src={user_logo} alt="users" className="footer__logo" />
        <div className="footer__info">
          <h4>Users Dashboard</h4>
          <p>Created by Olamide Olawuni</p>
        </div>
      </div>
      <div className="footer__center">
        <button className="footer__icon">
          <BiRefresh onClick={clearFilters} />
        </button>
      </div>
      <div className="footer__right">
        {/* <p>Filter and Search Users</p> */}
      </div>
    </div>
  );
};

export default Footer;
