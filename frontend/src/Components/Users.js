import React from "react";
//Components imported
import Body from "./Body";
import Footer from "./Footer";
import Sidebar from "./Sidebar";
import "./Users.css";

const Users = () => {
  return (
    <div className="users">
      <div className="users__body">
        <Sidebar />
        <Body />
      </div>
      <Footer />
    </div>
  );
};

export default Users;
