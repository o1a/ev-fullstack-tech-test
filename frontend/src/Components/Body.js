import React, { useEffect } from "react";
import { useGlobalContext } from "../context";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import "./Body.css";
import Header from "./Header";
import banner from "../images/users.jpg";
import { MdAddCircleOutline } from "react-icons/md";
// import { FiMoreHorizontal } from "react-icons/fi";
import User from "./User";
import Title from "./Title";
import Form from "./Form";
import { fetchUsers } from "../actions/userAsyncActions";

const Body = () => {
  const { openForm, setPostData, setCurrentID, currentID } = useGlobalContext();
  const dispatch = useDispatch();
  const users = useSelector((state) => state.usersReducers.users);
  const loading = useSelector((state) => state.usersReducers.loading);
  // console.log(loading);

  useEffect(() => {
    dispatch(fetchUsers());
  }, [currentID, dispatch]);

  const horizontalMain = () => {
    openForm();
    setCurrentID(null);
    setPostData({
      name: "",
      email: "",
      company: "",
      imageCover: "",
    });
  };

  return (
    <div className="body">
      <Header users={users} />
      <div className="body__info">
        <img src={banner} alt="" />
        <div className="body__infoText">
          <strong>USERS LIST</strong>
          <h2>Updated Weekly</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus
            perspiciatis alias, ipsa labore sint accusamus! Voluptate maxime quo
            aspernatur quod sequi numquam repudiandae labore debitis neque,
            tenetur laboriosam corporis ea.
          </p>
        </div>
      </div>
      <div className="body__users">
        <div className="body__icons">
          <MdAddCircleOutline
            fontSize="large"
            className="addUser"
            onClick={horizontalMain}
          />
          {/* <FiMoreHorizontal className="horizontal" onClick={horizontalMain} /> */}
        </div>
      </div>
      <Form />
      <Title />
      <hr />
      {/* List of Users */}
      {users.map((user) => {
        return <User key={user._id} {...user} loading={loading} />;
      })}
    </div>
  );
};

export default Body;
