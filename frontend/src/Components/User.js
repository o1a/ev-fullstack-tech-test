import React from "react";
import "./User.css";
// import image from "../images/person.jpg";
import { FiMoreHorizontal } from "react-icons/fi";
import { MdDeleteOutline } from "react-icons/md";
import { useGlobalContext } from "../context";
import { useDispatch } from "react-redux";
import { deleteUser } from "../actions/userAsyncActions";
// import { format } from "fecha";

const User = ({
  _id,
  name,
  email,
  imageCover,
  createdDate,
  company,
  loading,
}) => {
  const { openForm, setCurrentID } = useGlobalContext();
  const dispatch = useDispatch();

  //Open Form and set the current ID so that Form knows the clicked user and can make dispatch with the id
  const horizontalClick = () => {
    openForm();
    setCurrentID(_id);
  };

  if (loading) {
    <div className="loader"></div>;
  }

  return (
    <div className="userRow">
      <img
        src={imageCover}
        alt="No Image Available"
        className="userRow__image"
      />
      <div className="userRow__info">
        <div className="name">
          <h1>{name}</h1>
        </div>
        <div className="email">
          <p>{email}</p>
        </div>
        <div className="date">
          <p>{createdDate}</p>
        </div>
        <div className="company">
          <p>{company}</p>
        </div>
      </div>
      <div className="userRow__icons">
        <FiMoreHorizontal className="horizontal__1" onClick={horizontalClick} />
        <MdDeleteOutline
          className="horizontal__2 red_outline"
          onClick={() => dispatch(deleteUser(_id))}
        />
      </div>
    </div>
  );
};

export default User;
