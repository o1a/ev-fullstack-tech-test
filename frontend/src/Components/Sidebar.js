import React, { useState } from "react";
import "./Sidebar.css";
import logo from "../images/logo.jpg";
import SidebarOption from "./SidebarOption";
import { AiFillHome } from "react-icons/ai";
import { BiSearchAlt } from "react-icons/bi";
import { GiWalkingBoot } from "react-icons/gi";
import { Link } from "react-router-dom";
import { BiRefresh } from "react-icons/bi";
// import data from "../data";
import { useSelector } from "react-redux";
import { useGlobalContext } from "../context";

const Sidebar = () => {
  const { clearFilters } = useGlobalContext();
  const data = useSelector((state) => state.usersReducers.users);
  const allCompanies = [...new Set(data.map((item) => item.company))];

  return (
    <div className="sidebar">
      <img src={logo} alt="logo" className="sidebar__logo" />
      {/* <SidebarOption Icon={AiFillHome} title="Home" className="sidebar__icon" />
      <SidebarOption
        Icon={BiSearchAlt}
        title="Search"
        className="sidebar__icon"
      /> */}
      <Link to="/" className="sidebar__icon">
        <SidebarOption Icon={GiWalkingBoot} title="Sign Out" />
      </Link>
      <br />
      <div className="sidebar__header">
        <strong className="sidebar__title">COMPANIES</strong>
        <button className="footer__icon__top">
          <BiRefresh onClick={clearFilters} />
        </button>
      </div>
      <hr />
      {allCompanies.map((company, index) => {
        return <SidebarOption key={index} title={company} />;
      })}
    </div>
  );
};

export default Sidebar;
