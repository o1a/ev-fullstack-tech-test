import React, { useState, useEffect } from "react";
import { FaTimes } from "react-icons/fa";
import { useGlobalContext } from "../context";
import "./Form.css";
import { TextField, Button, Typography } from "@material-ui/core";
import FileBase from "react-file-base64";
import { useDispatch, useSelector } from "react-redux";
import { createUsers, updateUser } from "../actions/userAsyncActions";

const Form = () => {
  // const [postData, setPostData] = useState({
  //   name: "",
  //   email: "",
  //   company: "",
  //   imageCover: "",
  // });

  const {
    isFormOpen,
    closeForm,
    currentID,
    setCurrentID,
    postData,
    setPostData,
  } = useGlobalContext();

  const user = useSelector((state) =>
    currentID
      ? state.usersReducers.users.find((user) => user._id === currentID)
      : null
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (user) setPostData(user);
  }, [user]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (currentID) {
      dispatch(updateUser(currentID, postData));
    } else {
      dispatch(createUsers(postData));
    }
    clear();
  };

  const clear = () => {
    setCurrentID(null);
    setPostData({
      name: "",
      email: "",
      company: "",
      imageCover: "",
    });
  };
  return (
    // <div className={`modal-overlay show-modal`}>
    <div
      className={`${isFormOpen ? "modal-overlay show-modal" : "modal-overlay"}`}
    >
      <div className="modal-container">
        <form autoComplete="off" onSubmit={handleSubmit} className="form">
          <Typography variant="h6" style={{ color: "#001427" }}>
            {currentID ? "Edit" : "Create"} User
          </Typography>
          {/* <hr /> */}
          <br />
          <TextField
            name="name"
            variant="outlined"
            label="Name - Please enter a unique name"
            placeholder="Name"
            fullWidth
            style={{
              margin: "10px 0",
            }}
            value={postData.name}
            onChange={(e) => setPostData({ ...postData, name: e.target.value })}
          />
          <TextField
            name="email"
            variant="outlined"
            label="Email - Please enter a unique email"
            placeholder="Email"
            fullWidth
            style={{
              margin: "10px 0",
            }}
            value={postData.email}
            onChange={(e) =>
              setPostData({ ...postData, email: e.target.value })
            }
          />
          <TextField
            name="company"
            variant="outlined"
            label="Company - Please enter a company"
            placeholder="Company"
            fullWidth
            style={{
              margin: "10px 0",
            }}
            value={postData.company}
            onChange={(e) =>
              setPostData({ ...postData, company: e.target.value })
            }
          />
          <div className="fileInput">
            <FileBase
              type="file"
              multiple={false}
              onDone={({ base64 }) =>
                setPostData({ ...postData, imageCover: base64 })
              }
            />
          </div>
          <Button
            style={{
              backgroundColor: "#001427",
              color: "#fff",
              margin: "10px 0",
            }}
            className="buttonSubmit"
            size="large"
            type="submit"
            fullWidth
            onClick={closeForm}
          >
            Submit
          </Button>
          <Button
            style={{
              backgroundColor: "#BF0603",
              color: "#fff",
              marginBottom: "10px",
            }}
            className="buttonSubmit"
            size="small"
            onClick={clear}
            fullWidth
          >
            Clear
          </Button>
        </form>
        <button className="close-modal-btn" onClick={closeForm}>
          <FaTimes />
        </button>
      </div>
    </div>
  );
};

export default Form;
