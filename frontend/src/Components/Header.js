import React, { useEffect, useRef } from "react";
import "./Header.css";
import { BiSearchAlt } from "react-icons/bi";
import { HiUserCircle } from "react-icons/hi";
import { useGlobalContext } from "../context";

const Header = () => {
  const { setSearchTerm } = useGlobalContext();
  const searchValue = useRef("");

  useEffect(() => {
    searchValue.current.focus();
  }, []);

  const search = () => {
    setSearchTerm(searchValue.current.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className="header">
      <div className="header__left">
        <BiSearchAlt className="search_glass" />
        <form className="search-form" onSubmit={handleSubmit}>
          <input
            className="form-input"
            type="text"
            placeholder="Search for Users"
            ref={searchValue}
            onChange={search}
          />
        </form>
      </div>
      <div className="header__right">
        <HiUserCircle className="header__logo" />
        <h4>Olamide Olawuni</h4>
      </div>
    </div>
  );
};

export default Header;
