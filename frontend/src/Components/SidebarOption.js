import React from "react";
import { useDispatch } from "react-redux";
import { filter_Users } from "../actions/userActionTypes";
import "./SidebarOption.css";

function SidebarOption({ title, Icon }) {
  const dispatch = useDispatch();
  return (
    <div className="sidebarOption">
      {Icon && <Icon className="sidebarOption__icon" />}
      {Icon ? (
        <h4>{title}</h4>
      ) : (
        <button
          className="filter_buttons"
          onClick={() => dispatch(filter_Users(title))}
        >
          {title}
        </button>
      )}
    </div>
  );
}

export default SidebarOption;
