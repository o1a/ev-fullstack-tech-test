import React from "react";
import "./Title.css";

const Title = () => {
  return (
    <div className="title">
      <div className="title__user_name">
        <h4>NAME</h4>
      </div>
      <div className="title__user_email">
        <h4>EMAIL</h4>
      </div>
      <div className="title__user_date">
        <h4>DATE ADDED</h4>
      </div>
      <div className="title__user_company">
        <h4>COMPANY</h4>
      </div>
    </div>
  );
};

export default Title;
