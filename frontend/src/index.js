import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { AppProvider } from "./context";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import { usersReducers } from "./reducers/usersReducer";

// RootReducers for combining different reducers
// Store created using rootReducers and thunk middleware
const rootReducers = combineReducers({ usersReducers });
const store = createStore(rootReducers, compose(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store}>
    <AppProvider>
      <App />
    </AppProvider>
  </Provider>,
  document.getElementById("root")
);
